package com.yanzhen.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yanzhen.dao.OwnerMapper;
import com.yanzhen.model.House;
import com.yanzhen.model.Owner;
import com.yanzhen.service.IOwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kappy
 * @since 2020-11-08
 */
@Service
public class OwnerServiceImpl extends ServiceImpl<OwnerMapper, Owner> implements IOwnerService {

    @Autowired
    private OwnerMapper ownerDao;
    @Override
    public PageInfo<Owner> findOwnerAll(int page, int pagesize, Owner owner) {
        PageHelper.startPage(page,pagesize);
        List<Owner> list = new List<Owner>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Owner> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Owner owner) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Owner> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends Owner> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public Owner get(int index) {
                return null;
            }

            @Override
            public Owner set(int index, Owner element) {
                return null;
            }

            @Override
            public void add(int index, Owner element) {

            }

            @Override
            public Owner remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<Owner> listIterator() {
                return null;
            }

            @Override
            public ListIterator<Owner> listIterator(int index) {
                return null;
            }

            @Override
            public List<Owner> subList(int fromIndex, int toIndex) {
                return null;
            }
        };
        if(owner.getTel() == null && owner.getIdentity() == null && owner.getHouseId() == null)
            list=ownerDao.queryOwnerAll(owner);
        else
            list=ownerDao.queryOwner(owner.getTel(),owner.getIdentity(),owner.getHouseId());
        PageInfo<Owner> pageInfo=new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public PageInfo<Owner> findOwnerAll(int page, int pagesize, int house) {
        return null;
    }

    @Override
    public IPage<Owner> findListByPage(Integer page, Integer pageCount){
        IPage<Owner> wherePage = new Page<>(page, pageCount);
        Owner where = new Owner();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public Owner queryOwnerByName(String username) {
        return ownerDao.queryOwnerByName(username);
    }

    @Override
    public int add(Owner owner){
        return baseMapper.insert(owner);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(Owner owner){
        return baseMapper.updateById(owner);
    }

    @Override
    public Owner findById(Long id){
        return  baseMapper.selectById(id);
    }
}
