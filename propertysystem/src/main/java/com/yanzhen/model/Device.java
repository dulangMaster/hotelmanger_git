package com.yanzhen.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author kappy
 * @since 2021-03-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="House对象", description="")
public class Device implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private String devicedesc;

    private Integer status;

    private Integer required;

    public Integer getRequiredid() {
        return requiredid;
    }

    public void setRequiredid(Integer requiredid) {
        this.requiredid = requiredid;
    }

    private Integer requiredid;
    public Integer getRequired() {
        return required;
    }

    public void setRequired(Integer required) {
        this.required = required;
    }



    public void setDevicedesc(String devicedesc) {
        this.devicedesc = devicedesc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getOwerid() {
        return owerid;
    }

    public void setOwerid(Integer owerid) {
        this.owerid = owerid;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    private Integer owerid;


    private String remarks;


}
