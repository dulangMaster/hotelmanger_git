(function($){
	$.getData=function(name){
		var reg=new RegExp("(^|&)"+name+"=([^&]+)(&|$)?");
		var result = window.location.search.substr(1).match(reg);
		if (result!= null) return result[2]; return null;		
	}
})(jQuery);

var staffid=$.getData("dishid");
var pageNum=1;
var pageSize=8;
var choose;
var l;

$(document).ready(function(){
	getroomList();

	$('#dateStart').datepicker({
		language: 'zh-CN',
		format: 'yyyy-mm-dd',
		autoclose: true
	}).on('changeDate',function(e){
		var startTime = e.date;
		$('#dateEnd').datepicker('setStartDate',startTime);
	});

	$('#dateEnd').datepicker({
		language: 'zh-CN',
		format: 'yyyy-mm-dd',
		autoclose: true
	}).on('changeDate',function(e){
		var endTime = e.date;
		$('#dateStart').datepicker('setEndDate',endTime);
	});


	$("#pre").on('click',function(){
		getPre();
	});
	$("#next").on('click',function(){
		getNext();
	});
	$("#addOrder").on('click',function(){
		addOrder();
	});
    $("#searchBtn").on('click',function(){
        getListSearch();
    });

})



//判断对象/JSON是否为空 空返回1 非空返回0
function isEmptyObject(e) {
	var t;
	for (t in e)
		return 0;
	return 1;
}

//判断字符串是否为空 空返回1 非空返回0
function isEmptyString(str){
	if(str=='null'||str=='')
		return 1;
	return 0;
}


var list;
function getListSearch(){
    var serSearch1 = $("#searchRoom").val();
    var serSearch2 = $("#searchMoney").val();
    $.ajax({
        type:"post",
        url:"../dish/getDishes",
        dataType:"JSON",
        data:{
            "state":"1",
            "type":"-1",
            "pageNum":pageNum,
            "pageSize":pageSize,
            "state": 0,
            "serSearch1":serSearch1,
            "serSearch2":serSearch2
        },
        success:function(data){
            if(isEmptyObject(data.List)&&pageNum>0){
                pageNum=pageNum-1;
                getListSearch();
            }
            else{

                list=data.List;
                var power=" ";
                var htmlStr=" ";
                var btnStr=" ";
                var state=" ";
                var type=" ";
                l=0;
                $("#pre").css("display","block");
                $("#next").css("display","block");
                $("#roomList").empty();
                $("#roomList").append("<tr><th>菜单</th><th>图片</th><th>价格</th><th>状态</th><th>当前存量</th><th>操作</th></tr>");
                for(i in list){
                    var url = '../'+list[i].imgurl;
                    if(list[i].state=="0"){
                        type="今日提供"
                    }else
                        type="今日不提供";
                    btnStr= "<input type=\"button\" id=\"chooseRoomBtn\" data-dishid=\""+list[i].dishid+"\" class=\"btn btn-success\" value=\"立即购买\"/>";
                    htmlStr="<tr data-dishid=\""+list[i].dishid+"\"><td>"+list[i].dishname+"</td><td><img style='width: 130px;height: 70px' src='"+url+"'+/><td>"+list[i].money+"</td><td>"+type+"</td><td>"+list[i].number+"份"+"</td><td>"+btnStr+"</td></tr>";
                    $("#roomList").append(htmlStr);
                    l++;
                }
                if(pageNum=="1") $("#pre").css("display","none");
                if(pageSize>l) $("#next").css("display","none");
                btnOn();
            }

        },
        error:function(){
            alert("获取菜单列表发生错误")
        }
    })
}
function getroomList(){
	$.ajax({
		type:"post",
		url:"../dish/getDishes",
		dataType:"JSON",
		data:{
			"state":"1",
			"type":"-1",
			"pageNum":pageNum,
			"pageSize":pageSize,
			"state": 0
		},
		success:function(data){
			if(isEmptyObject(data.List)&&pageNum>0){
				pageNum=pageNum-1;
				getroomList();
			}
			else{

				list=data.List;
				var power=" ";
				var htmlStr=" ";
				var btnStr=" ";
				var state=" ";
				var type=" ";
				l=0;
				$("#pre").css("display","block");
				$("#next").css("display","block");
				$("#roomList").empty();
				$("#roomList").append("<tr><th>菜单</th><th>图片</th><th>价格</th><th>状态</th><th>当前存量</th><th>操作</th></tr>");
				for(i in list){
                    var url = '../'+list[i].imgurl;
					if(list[i].state=="0"){
                        type="今日提供"
					}else
						type="今日不提供";
                    btnStr= "<input type=\"button\" id=\"chooseRoomBtn\" data-dishid=\""+list[i].dishid+"\" class=\"btn btn-success\" value=\"立即购买\"/>";
					htmlStr="<tr data-dishid=\""+list[i].dishid+"\"><td>"+list[i].dishname+"</td><td><img style='width: 130px;height: 70px' src='"+url+"'+/><td>"+list[i].money+"</td><td>"+type+"</td><td>"+list[i].number+"份"+"</td><td>"+btnStr+"</td></tr>";
					$("#roomList").append(htmlStr);
					l++;
				}
				if(pageNum=="1") $("#pre").css("display","none");
				if(pageSize>l) $("#next").css("display","none");
				btnOn();
			}

		},
		error:function(){
			alert("获取菜单列表发生错误")
		}
	})
}

function btnOn(){
	$("input").filter("#setPageBtn").on('click',function( ){
		setPage( );
	});
	$("input").filter("#chooseRoomBtn").on('click',function(event){
		chooseRoomBtn(event);
	});
}

function getPre(){
	pageNum=pageNum-1;
	getroomList();
}

function getNext(){
	pageNum=pageNum+1;
	getroomList();	
}

function setPage(){
	if($("#inputPage").val()<0 || $("#inputPage").val()==0)
		alert("请输入正确页码");
	else{
		pageNum=$("#inputPage").val();
		getroomList();
	}
	
}

function chooseRoomBtn(event){
    var dishid=$(event.target).data("dishid");
    $.ajax({
        type:"POST",
        url:"../dish/alterdish",
        dataType:"JSON",
        data:{
            "orderid":dishid
        },
        success:function(data){
            if(data.code==0){
                alert("购买成功");
                if(l==1)
                    pageNum=pageNum-1;
                getroomList();
            }
            else
                alert(data.message)
        },
        error:function(){
            alert("系统出现错误");
        }
    })
    
}

function addOrder(){
	if(isEmptyString($("#inputName").val())||isEmptyString($("#inputId").val())||isEmptyString($("#dateStart").val())||isEmptyString($("#dateEnd").val()))
		alert("请填写全内容");
	else{
		$.ajax({
			type:"POST",
			url:"../order/addOrder.do",
			dataType:"JSON",
			data:{
				"dishid":staffid,
				"dishid":choose,
				"householdname":$("#inputName").val(),
				"id":$("#inputId").val(),
				"starttime":$("#dateStart").val(),
				"endtime":$("#dateEnd").val()
			},
			success:function(data){
				if(data.code==0){
					alert("添加成功");
					$("#inputRoom").val("");
					$("#inputName").val("");
					$("#inputId").val("");
					$("#dateStart").val("");
					$("#dateEnd").val("");
					$('#chooseRoom').modal('toggle');
					window.location.href="staff_order.html";
				}
			},
			error:function(){
				alert("添加订单出现错误");
			}
		})
	}
	

}

