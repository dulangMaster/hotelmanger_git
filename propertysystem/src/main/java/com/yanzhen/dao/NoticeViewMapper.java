package com.yanzhen.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.pagehelper.PageInfo;
import com.yanzhen.model.Notice;
import com.yanzhen.model.NoticeView;
import com.yanzhen.model.Userinfo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kappy
 * @since 2020-11-08
 */
public interface NoticeViewMapper extends BaseMapper<NoticeView> {
    List<NoticeView> queryUserinfoAll();
    NoticeView findmax(Long id);
}
