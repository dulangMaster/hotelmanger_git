package com.yanzhen.controller;

import com.yanzhen.model.Owner;
import com.yanzhen.model.Userinfo;
import com.yanzhen.service.IOwnerService;
import com.yanzhen.service.IUserinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class LoginController {
    @Autowired
    private IUserinfoService userinfoService;
    @Autowired
    private IOwnerService ownerService;
    @RequestMapping("/loginIn")
    @ResponseBody
    public Map loginIn(Userinfo userinfo, HttpServletRequest request) {
        Map map = new HashMap();
        HttpSession session = request.getSession();
        Userinfo user;
        if (session == null) {
            map.put("code", 404);
            map.put("msg", "登录超时了");
            return map;
        }
        if (userinfo.getType()==2){
            userinfo.setType(0);
            userinfo.setIsower(1);
            user = userinfoService.queryUserByNameAndPwdAndOwer(userinfo);
        } else {
            userinfo.setIsower(0);
            user = userinfoService.queryUserByNameAndPwd(userinfo);
        }
        System.out.println(">>>>>>>>>>>user" + user + (user == null));
        if (user == null) {
            map.put("code", 404);
            map.put("msg", "用户名或者密码错误");
            return map;
        } else {
            session.setAttribute("user", user);
            map.put("code", 200);
            map.put("user", user);
            map.put("username", user.getUsername());
            return map;
        }
    }

    @RequestMapping("/registerIn")
    @ResponseBody
    public Map registerIn(Userinfo userinfo, HttpServletRequest request) {
        Map map = new HashMap();
        Userinfo user = userinfoService.queryUserByName(userinfo);
        if (user != null) {
            map.put("code", 404);
            map.put("msg", "用户名已经存在");
            return map;
        }
        Owner owner = new Owner();
        owner.setPassword(userinfo.getPassword());
        owner.setUsername(userinfo.getUsername());
        owner.setHouseId(String.valueOf(1));
        if (Integer.parseInt(userinfo.getSex()) == 0){
            owner.setSex("男");
        } else {
            owner.setSex("女");
        }
        owner.setTel(userinfo.getTel());
        ownerService.add(owner);
        int flag;
        if (userinfo.getType() == 2) {
            userinfo.setIsower(1);
            userinfo.setType(0);
            flag = userinfoService.add(userinfo);
        } else {
            userinfo.setIsower(0);
            flag = userinfoService.add(userinfo);
        }

        System.out.println(">>>>>>>>>>>" + flag);
        if (flag == 0) {
            map.put("code", 404);
            map.put("msg", "注册失败");
            return map;
        } else {
            map.put("code", 200);
            map.put("msg", "注册成功");
            return map;
        }

    }

    /**
     * 退出功能
     */
    @RequestMapping("/loginOut")
    public void loginOut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        session.invalidate();
        response.sendRedirect(request.getContextPath() + "/login.html");
    }
}
