package com.yanzhen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.yanzhen.model.Device;
import com.yanzhen.model.NoticeView;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kappy
 * @since 2020-11-08
 */
public interface IDeviceService extends IService<Device> {
    PageInfo<Device> findOwnerAll(int page, int pagesize, Device owner);
    /**
     * 查询分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<Notice>
     */
    PageInfo<Device> findListByPage(Integer page, Integer pageCount);

    int add(Device owner);
    int updateData(Device owner);
    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);
    Device findById(Long id);
    void updateOwer(Integer id);

}
