package com.jiudian.manage.service.impl;

import com.github.pagehelper.PageHelper;
import com.jiudian.manage.mapper.RoomMapper;
import com.jiudian.manage.model.Room;
import com.jiudian.manage.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {
    @Autowired
    RoomMapper roomMapper;

    @Override
    public boolean addRoom(String file,String local, double money, int state, int type) {
        Room room = new Room();
        room.setLocal(local);
        room.setImgurl(file);
        room.setMoney(money);
        room.setState(state);
        room.setType(type);
        System.out.println(">>>>>file" + file + room);
        int i = roomMapper.insertSelective(room);
        return i>0?true:false;
    }

    @Override
    public boolean delRoom(int roomid) {
        int i = roomMapper.deleteByPrimaryKey(roomid);
        return i>0?true:false;
    }

    @Override
    public boolean updateRoom(int roomid, String local, double money, int state, int type) {
        Room room = new Room();
        room.setRoomid(roomid);
        if(!local.equals("null")){
            room.setLocal(local);
        }
        if(money!=-1){
            room.setMoney(money);
        }
        if(state!=-1){
            room.setState(state);
        }
        if(type!=-1){
            room.setType(type);
        }
        int i = roomMapper.updateByPrimaryKeySelective(room);
        return i>0?true:false;
    }

    @Override
    public boolean updateRoomState(int roomid, int state) {
        Room room = new Room();
        room.setRoomid(roomid);
        room.setState(state);
        int i = roomMapper.updateByPrimaryKeySelective(room);
        return i>0?true:false;
    }

    @Override
    public List<Room> getRoomByState(int state, int type,int pageNum,int pageSize,String roomname,Double money) {
        Room room = new Room();
        if(state!=-1){
            room.setState(state);
        }
        if(type!=-1){
            room.setType(type);
        }
        if(roomname!=null){
            room.setLocal(roomname);
        }
        if(money!=-1){
            room.setMoney(money);
        }
        PageHelper.startPage(pageNum,pageSize);
        System.out.println("+++++++"+room);
        return roomMapper.selectRoomByStateType(room);
    }

    @Override
    public Room getRoomById(int roomid) {
        return roomMapper.selectByPrimaryKey(roomid);
    }
}
