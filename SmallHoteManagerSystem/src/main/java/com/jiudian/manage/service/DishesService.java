package com.jiudian.manage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jiudian.manage.model.Dishes;
import com.jiudian.manage.model.DishesList;

import java.util.List;

public interface DishesService {
    /**
     * 增加订单
     * @param householdname
     * @param id
     * @param starttime
     * @param endtime
     * @param roomid
     * @param userid
     * @return
     */
    public boolean addOrder(String householdname, String id, String starttime, String endtime, int roomid, int userid);

    public boolean addOrder(Dishes dishes);

    /**
     * 删除订单
     * @param orderid
     * @return
     */
    public boolean delOrder(int orderid);

    /**
     * 修改订单状态
     * @param orderid
     * @param state
     * @return
     */
    public boolean updateOrderState(int orderid, int state);
    public boolean updateDish(Dishes dishes);

    /**
     *获取所有订单
     * @return
     */
    public List<DishesList> getAllOrder(int userid, int pageNum, int pageSize);

    public IPage<Dishes> getDishes(int pageNum, int pageSize, int state,String name,Double money);
    public Dishes getDishesById(int id);
}
