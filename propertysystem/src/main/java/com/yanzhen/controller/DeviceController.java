package com.yanzhen.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.pagehelper.PageInfo;
import com.yanzhen.model.*;
import com.yanzhen.service.IDeviceService;
import com.yanzhen.service.INoticeService;
import com.yanzhen.service.INoticeViewService;
import com.yanzhen.util.JsonObject;
import com.yanzhen.util.R;
import com.yanzhen.util.StatusDefind;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author kappy
 * @since 2020-11-08
 */
@Api(tags = {""})
@RestController
@RequestMapping("/device")
public class DeviceController {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Resource
    private INoticeService noticeService;

    @Resource
    private IDeviceService noticeViewService;

    @ApiOperation(value = "新增")
    @PostMapping()
    public int add(@RequestBody Notice notice) {
        return noticeService.add(notice);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public int delete(@PathVariable("id") Long id) {
        return noticeService.delete(id);
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public int update(@RequestBody Notice notice) {
        return noticeService.updateData(notice);
    }

    @ApiOperation(value = "查询分页数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码"),
            @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping()
    public IPage<Notice> findListByPage(@RequestParam Integer page,
                                        @RequestParam Integer pageCount) {
        return noticeService.findListByPage(page, pageCount);
    }

    @RequestMapping("/queryUserInfoAll")
    public JsonObject<NoticeView> findListByPage2(@RequestParam Integer page,
                                                  @RequestParam Integer limit) {
        JsonObject object = new JsonObject();
        PageInfo<Device> pageInfo = noticeViewService.findListByPage(page, limit);
        object.setCode(0);
        object.setMsg("ok");
        object.setCount(pageInfo.getTotal());
        object.setData(pageInfo.getList());
        return object;

    }
    @RequestMapping("/queryAll")
    @Transactional
    public List queryAll(){
        PageInfo<Device> pageInfo= noticeViewService.findOwnerAll(1,100,null);
        return pageInfo.getList();
    }
    @ApiOperation(value = "新增")
    @RequestMapping("/add")
    public R add(@RequestBody Device owner) {
        //md5加密
//        String md5Password = DigestUtils.md5DigestAsHex("123456".getBytes());

        int num = noticeViewService.add(owner);
        if (num > 0) {
            return R.ok();
        } else {
            return R.fail("添加失败");
        }
    }
        @ApiOperation(value = "删除")
        @RequestMapping("/deleteByIds")
        public R delete(String  ids){
            List<String> list= Arrays.asList(ids.split(","));
            //遍历遍历进行删除
            for(String id:list){
                noticeViewService.delete(Long.parseLong(id));
            }
            return R.ok();
        }
    @ApiOperation(value = "删除")
    @RequestMapping("/update")
    public Map update(Long  ids,int status, HttpServletRequest request){
        // 同意 1,不同意 2,解除 3,申请 4
        Device device = noticeViewService.findById(ids);
        System.out.println(">>>>>>>>>>status." +status);
        HttpSession session = request.getSession();
        Userinfo userinfo = (Userinfo) session.getAttribute("user");
        if(status == 1){
            device.setRequired(0);
            device.setOwerid(device.getRequiredid());
        } else if (status == 2){
            device.setRequired(2);
        }else if (status == 4){
            device.setRequiredid(userinfo.getId());
            device.setStatus(1);
            device.setRequired(1);

        }
        Map map = new HashMap();
        map.put("code", 200);
        map.put("msg", "成功");
        if (device != null){
            noticeViewService.updateData(device);
        }
        if (status == 3) {
            System.out.println("3333333." + status);
            device.setStatus(0);
            noticeViewService.updateData(device);
            noticeViewService.updateOwer(device.getId());
        }
            return map;
    }
    @ApiOperation(value = "id查询")
    @RequestMapping("/allmax")
    public void allmax() throws InterruptedException {
        Long aLong= noticeService.findMax();
        StatusDefind.Noticeid = aLong;
        System.out.println("++++++++++allmax"+ aLong);

    }

    @ApiOperation(value = "id查询")
    @RequestMapping("/checkStatus")
    public Map maxById() throws InterruptedException {

        return null;
    }

    @ApiOperation(value = "id查询")
    @RequestMapping("/changeStatus")
    public void findById(@RequestParam int status) throws InterruptedException {
        System.out.println(">>>>>>>>>>" + status);
        Map map = new HashMap();

        if (status == 1) {
            StatusDefind.status = 1;
        } else {
            StatusDefind.status = 0;
        }
        Random r = new Random(1);
        while (StatusDefind.status == 1) {
            System.out.println(">>>>>>>>>>" + StatusDefind.status);
            int ran1 = r.nextInt(100);
            int ran2 = r.nextInt(5);
            ran2 = ran2 == 0 ? 1 : ran2;
            if (ran1 >= 95) {
                Notice notice = new Notice();
                notice.setContent("test" + ran1);
                notice.setFbdate(new Date());
                notice.setIdhouse(ran2);
                notice.setStatus(3);
                noticeService.add(notice);
            } else if (ran1 >= 90) {
                Notice notice = new Notice();
                notice.setContent("test" + ran1);
                notice.setFbdate(new Date());
                notice.setIdhouse(ran2);
                notice.setStatus(2);
                noticeService.add(notice);
            } else if (ran1 <= 90 && ran1 >= 85) {
                Notice notice = new Notice();
                notice.setContent("test" + ran1);
                notice.setFbdate(new Date());
                notice.setIdhouse(ran2);
                notice.setStatus(1);
                noticeService.add(notice);
            }
            Thread.currentThread().sleep(2000);
        }
    }


}
