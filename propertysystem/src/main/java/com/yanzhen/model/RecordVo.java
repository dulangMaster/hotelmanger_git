package com.yanzhen.model;

import lombok.Data;

@Data
public class RecordVo extends  Records{
    //户主
    private String username;
    //房屋ID
    private String houseId;
    //费用类型
    private String typename;


}
