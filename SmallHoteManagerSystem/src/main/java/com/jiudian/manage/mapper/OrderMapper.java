package com.jiudian.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiudian.manage.model.Order;
import com.jiudian.manage.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderMapper extends BaseMapper<Order> {
    int deleteByPrimaryKey(Integer orderid);

    int insertOrder(Order record);

    int insertSelective(Order record);
    int selectUserMoney(Integer userid);

    Order selectByPrimaryKey(Integer orderid);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);

    List<Order> getAllUser(User user);
}