/*
 Navicat Premium Data Transfer

 Source Server         : 61
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : 172.20.17.61:3306
 Source Schema         : HotelManager

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 30/04/2021 10:17:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `managesalary` double(255, 0) NULL DEFAULT NULL,
  `staffsalary` double(255, 0) NULL DEFAULT NULL,
  `cleanerssalary` double(255, 0) NULL DEFAULT NULL,
  `manage` double(255, 0) NULL DEFAULT NULL,
  `staff` double(255, 0) NULL DEFAULT NULL,
  `totalroom` double(255, 0) NULL DEFAULT NULL,
  `cleaner` double(255, 0) NULL DEFAULT NULL,
  `totalmoney` double(255, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES (1, 50, 10, 5, 100, 100, 127, 100, 54410);

-- ----------------------------
-- Table structure for dishes
-- ----------------------------
DROP TABLE IF EXISTS `dishes`;
CREATE TABLE `dishes`  (
  `dishid` bigint(12) NOT NULL AUTO_INCREMENT,
  `dishname` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` bigint(11) NULL DEFAULT 0,
  `money` bigint(20) NULL DEFAULT 0,
  `number` bigint(11) NULL DEFAULT 20,
  `imgurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`dishid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dishes
-- ----------------------------
INSERT INTO `dishes` VALUES (1, '酸菜鱼', 0, 20, 15, 'img/04.jpg');
INSERT INTO `dishes` VALUES (2, '麻婆豆腐', 0, 12, 14, 'img/01.jpg');
INSERT INTO `dishes` VALUES (4, '鱼香肉丝', 0, 15, 14, 'img/02.jpg');
INSERT INTO `dishes` VALUES (5, '烤鸭', 0, 35, 16, 'img/03.jpg');
INSERT INTO `dishes` VALUES (6, '炖鸡汤', 1, 30, 17, 'img/05.jpg');
INSERT INTO `dishes` VALUES (11, '回锅肉', 0, 10, 18, 'img/07.jpg');
INSERT INTO `dishes` VALUES (12, '青椒肉丝', 0, 28, 19, 'File\\64bf69c5-2c0e-42c1-9aa2-3bf08e2e61f2ss.jpg');

-- ----------------------------
-- Table structure for dishlist
-- ----------------------------
DROP TABLE IF EXISTS `dishlist`;
CREATE TABLE `dishlist`  (
  `orderid` bigint(11) NOT NULL AUTO_INCREMENT,
  `money` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userid` int(11) NULL DEFAULT NULL,
  `dishid` int(11) NULL DEFAULT NULL,
  `state` bigint(11) NULL DEFAULT NULL,
  `number` bigint(11) NULL DEFAULT NULL,
  PRIMARY KEY (`orderid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dishlist
-- ----------------------------
INSERT INTO `dishlist` VALUES (1, '40', 4, 1, 0, 2);
INSERT INTO `dishlist` VALUES (4, '15.0', 4, 4, NULL, 1);
INSERT INTO `dishlist` VALUES (7, '70.0', 4, 5, NULL, 2);
INSERT INTO `dishlist` VALUES (8, '12.0', 4, 2, NULL, 1);
INSERT INTO `dishlist` VALUES (9, '30.0', 6, 6, NULL, 1);
INSERT INTO `dishlist` VALUES (10, '30.0', 6, 4, NULL, 2);
INSERT INTO `dishlist` VALUES (11, '35.0', 6, 5, NULL, 1);
INSERT INTO `dishlist` VALUES (12, '20.0', 6, 1, NULL, 1);
INSERT INTO `dishlist` VALUES (13, '24.0', 6, 2, NULL, 2);
INSERT INTO `dishlist` VALUES (14, '12.0', 13, 2, NULL, 1);
INSERT INTO `dishlist` VALUES (15, '20.0', 17, 1, NULL, 1);

-- ----------------------------
-- Table structure for orderlist
-- ----------------------------
DROP TABLE IF EXISTS `orderlist`;
CREATE TABLE `orderlist`  (
  `orderid` int(11) NOT NULL AUTO_INCREMENT,
  `householdname` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `starttime` date NULL DEFAULT NULL,
  `endtime` date NULL DEFAULT NULL,
  `money` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roomid` int(11) NULL DEFAULT NULL,
  `userid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`orderid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orderlist
-- ----------------------------
INSERT INTO `orderlist` VALUES (1, 'admin', '1221', '2021-04-28', '2021-05-04', '600.0', '3', 1, 1);
INSERT INTO `orderlist` VALUES (2, 'test', '122', '2021-04-28', '2021-04-30', '400.0', '2', 2, 1);
INSERT INTO `orderlist` VALUES (3, 'test', '11', '2021-04-28', '2021-04-30', '300.0', '2', 3, 2);
INSERT INTO `orderlist` VALUES (5, 'test', '1221', '2021-05-07', '2021-05-19', '1800.0', '2', 7, 2);
INSERT INTO `orderlist` VALUES (6, 'test', '11', '2021-04-20', '2021-05-01', '1100.0', '2', 1, 2);
INSERT INTO `orderlist` VALUES (7, 'test', '11122', '2021-04-28', '2021-04-29', '110.0', '2', 4, 2);
INSERT INTO `orderlist` VALUES (8, 'test2', '522221111111', '2021-04-06', '2021-04-29', '2300.0', '2', 1, 6);
INSERT INTO `orderlist` VALUES (9, 'test2', '1122', '2021-03-29', '2021-04-29', '3100.0', '2', 5, 6);
INSERT INTO `orderlist` VALUES (10, 'test2', '1222111', '2021-04-27', '2021-04-27', '0.0', '2', 6, 6);
INSERT INTO `orderlist` VALUES (11, 'test2', '522221111111', '2021-04-28', '2021-04-30', '200.0', '2', 1, 6);
INSERT INTO `orderlist` VALUES (12, 'test2', '522221111111', '2021-05-04', '2021-05-27', '2300.0', '2', 5, 6);
INSERT INTO `orderlist` VALUES (13, 'test2', '522221111111', '2021-05-04', '2021-05-23', '3800.0', '2', 6, 6);
INSERT INTO `orderlist` VALUES (14, 'test2', '522221111111', '2021-04-20', '2021-04-30', '2000.0', '2', 2, 6);
INSERT INTO `orderlist` VALUES (15, 'test2', '111', '2021-04-14', '2021-04-29', '3000.0', '3', 6, 6);
INSERT INTO `orderlist` VALUES (16, 'test2', '1111221111', '2021-04-13', '2021-04-29', '2400.0', '3', 7, 6);
INSERT INTO `orderlist` VALUES (17, 'admin', '11112222333', '2021-04-21', '2021-04-30', '1800.0', '3', 2, 6);
INSERT INTO `orderlist` VALUES (18, 'test2', '12331111111', '2021-03-30', '2021-04-29', '6000.0', '3', 2, 6);
INSERT INTO `orderlist` VALUES (19, 'test2', '111', '2021-04-21', '2021-04-29', '1600.0', '3', 6, 4);
INSERT INTO `orderlist` VALUES (20, 'test', '522221111111233221', '2021-04-20', '2021-04-29', '900.0', '2', 1, 4);
INSERT INTO `orderlist` VALUES (21, 'test66', '522221111111322221', '2021-04-21', '2021-05-22', '6200.0', '2', 2, 13);
INSERT INTO `orderlist` VALUES (22, 'test1ss', '52222111111112233111', '2021-04-01', '2021-04-02', '100.0', '0', 1, 17);
INSERT INTO `orderlist` VALUES (23, 'test1ss', '522221111111233443', '2021-04-01', '2021-04-10', '1800.0', '0', 2, 17);

-- ----------------------------
-- Table structure for room
-- ----------------------------
DROP TABLE IF EXISTS `room`;
CREATE TABLE `room`  (
  `roomid` int(11) NOT NULL AUTO_INCREMENT,
  `local` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `money` double(255, 0) NULL DEFAULT NULL,
  `state` bigint(255) NULL DEFAULT NULL,
  `type` bigint(255) NULL DEFAULT NULL,
  `imgurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`roomid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of room
-- ----------------------------
INSERT INTO `room` VALUES (1, '201号', 100, 2, 1, 'img/4.jpg');
INSERT INTO `room` VALUES (2, '202号', 200, 2, 4, 'img/2.jpg');
INSERT INTO `room` VALUES (3, '203号', 150, 1, 1, 'img/3.jpg');
INSERT INTO `room` VALUES (4, '204号', 110, 1, 2, 'img/11.jpg');
INSERT INTO `room` VALUES (5, '205号', 100, 1, 2, 'img/6.jpg');
INSERT INTO `room` VALUES (6, '206号', 200, 1, 2, 'img/7.jpg');
INSERT INTO `room` VALUES (7, '101号', 150, 1, 1, 'img/8.jpg');
INSERT INTO `room` VALUES (12, '308号', 100, 1, 1, 'File\\83b3c125-2739-4e95-ba28-a442ffcc60db11.jpg');
INSERT INTO `room` VALUES (13, '808号', 122, 1, 2, 'File\\33f3c49b-9e7f-44bd-8e1c-d5e3c87d2d1599.jpg');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `userid` bigint(11) NOT NULL AUTO_INCREMENT,
  `useraccount` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `power` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IDnumber` int(11) NULL DEFAULT NULL,
  `money` double(255, 0) NULL DEFAULT NULL,
  `photoUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phonenumber` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` bigint(2) NULL DEFAULT 0,
  `gender` bigint(255) NULL DEFAULT 0,
  PRIMARY KEY (`userid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', '1', 60, '1', 1, 5, 'File\\9f8e7bb9-be42-483e-a499-9a2f384edef2test2.jpg', '554114', 'admin', 2, 0);
INSERT INTO `user` VALUES (4, 'test', '1', 45, '0', NULL, 9174, 'File\\03d10545-1c18-4c94-80a4-0d366a09e480test.jpeg', '8771554114', '用户1', 1, 0);
INSERT INTO `user` VALUES (6, 'test2', '1', 20, '0', NULL, 7966, 'File\\d5cf8e77-c42b-48f0-b186-dbc47980bcddaaa.jpg', '18322111777', '用户2', 1, 0);
INSERT INTO `user` VALUES (7, 'test4', 'test4', 112, '0', NULL, 1000, NULL, '1544895555', '用户3', 0, 0);
INSERT INTO `user` VALUES (8, 'test5', 'test5', 11, '0', NULL, 8005, NULL, '1522224445', '张三', 1, 0);
INSERT INTO `user` VALUES (9, 'aaa', 'aaa', 10, '0', NULL, 9000, NULL, '22112222', '李四', 1, 0);
INSERT INTO `user` VALUES (10, 'dda', 'dda', 10, '0', NULL, 9000, NULL, '551122111', '王二', 1, 0);
INSERT INTO `user` VALUES (11, 'aad', '8881', 10, '0', NULL, 9000, NULL, '1557444422', '王飞', 1, 0);
INSERT INTO `user` VALUES (12, 'sdsd', 'asasa', 11, '0', NULL, 1111, NULL, '151871111177', '李师师', 0, 0);
INSERT INTO `user` VALUES (13, 'test66', 'test66', 12, '0', NULL, 13188, NULL, '1823388811', '李师请', 1, 0);
INSERT INTO `user` VALUES (14, '11221test', '2221', 11, '0', NULL, 122, NULL, '2111111111', '11', 0, 0);
INSERT INTO `user` VALUES (15, 'testimg', 'testimg', 10, '0', NULL, 122, 'File\\98284ca2-9495-49e0-884f-6376781764e2my.jpg', '123342222', 'testimg', 0, 0);
INSERT INTO `user` VALUES (16, 'aaass', 'asssa', 40, '0', NULL, 100, 'File\\0bd3e05a-2e51-499f-a2c8-d58b97a0c57bmy.jpg', '8771554114', '王青', 0, 0);
INSERT INTO `user` VALUES (17, 'test1ss', 'test1ss', 15, '0', NULL, 9200, 'File\\21f798f5-f159-47a0-ab64-fd8dcd4aec3c99.jpg', '1233222232', '李想', 1, 0);

SET FOREIGN_KEY_CHECKS = 1;
