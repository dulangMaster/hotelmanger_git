package com.yanzhen.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yanzhen.model.Device;
import com.yanzhen.model.NoticeView;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kappy
 * @since 2020-11-08
 */
public interface DeviceMapper extends BaseMapper<Device> {
    List<Device> queryUserinfoAll();
    NoticeView findmax(Long id);
    void updateOwer(Integer id);
}
