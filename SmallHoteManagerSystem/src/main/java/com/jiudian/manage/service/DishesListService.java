package com.jiudian.manage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jiudian.manage.model.Dishes;
import com.jiudian.manage.model.DishesList;
import com.jiudian.manage.model.User;

import java.util.List;

public interface DishesListService {
    boolean updateOrder(User userid, Dishes dishid);
}
