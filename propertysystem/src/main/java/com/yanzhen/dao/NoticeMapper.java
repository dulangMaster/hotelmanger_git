package com.yanzhen.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yanzhen.model.Notice;
import com.yanzhen.model.NoticeView;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kappy
 * @since 2020-11-08
 */
public interface NoticeMapper extends BaseMapper<Notice> {
    public Notice findMaxId(Long id);

    public Long findMax();
}
