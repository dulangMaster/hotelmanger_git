package com.jiudian.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiudian.manage.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface UserMapper extends BaseMapper<User> {
    int deleteByPrimaryKey(Integer userid);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userid);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<User> getAllUser();

    List<User> selectByPower(User user);

    User selectByAccount(String useraccount);
}