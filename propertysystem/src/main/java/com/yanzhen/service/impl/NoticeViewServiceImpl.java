package com.yanzhen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yanzhen.dao.NoticeViewMapper;
import com.yanzhen.model.NoticeView;
import com.yanzhen.service.INoticeViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kappy
 * @since 2020-11-08
 */
@Service
public class NoticeViewServiceImpl extends ServiceImpl<NoticeViewMapper, NoticeView> implements INoticeViewService {

    @Autowired
    private NoticeViewMapper noticeViewMapper;
    public NoticeView findmax(Long id){
      return  noticeViewMapper.findmax(id);
    }
    public PageInfo<NoticeView> findListByPage(Integer page, Integer pageSize) {

        PageHelper.startPage(page,pageSize);
        //查询的结果集
        List<NoticeView> list=noticeViewMapper.queryUserinfoAll();
        PageInfo<NoticeView> pageInfo=new PageInfo<>(list);
        return  pageInfo;

    }

}
