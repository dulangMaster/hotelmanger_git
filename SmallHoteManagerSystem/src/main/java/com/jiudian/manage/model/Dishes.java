package com.jiudian.manage.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;
@Data
public class Dishes {
    @TableId(value = "dishid", type = IdType.AUTO)
    private Integer dishid;

    private String dishname;
    private String imgurl;
    private Double money;

    private Integer state;

    private Integer number;



}