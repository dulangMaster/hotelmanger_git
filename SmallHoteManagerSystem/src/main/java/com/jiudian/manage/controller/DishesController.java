package com.jiudian.manage.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jiudian.manage.model.Dishes;
import com.jiudian.manage.model.DishesList;
import com.jiudian.manage.model.Order;
import com.jiudian.manage.model.User;
import com.jiudian.manage.service.DishesListService;
import com.jiudian.manage.service.DishesService;
import com.jiudian.manage.service.UserService;
import com.jiudian.manage.service.impl.OrderServiceImpl;
import com.jiudian.manage.until.FileUtil;
import com.jiudian.manage.until.State;
import com.jiudian.manage.until.StateSignal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "/dish")
public class DishesController {
    @Autowired
    DishesService orderService;
    @Autowired
    OrderServiceImpl roomOrder;
    @Autowired
    DishesListService dishesListService;

    @Autowired
    UserService userService;

    /**
     * 添加订单
     *
     * @param householdname 入住人姓名
     * @param id            入住人身份证号
     * @param starttime     开始时间
     * @param endtime       结束时间
     * @param roomid        房间id
     * @param userid        用户id
     * @return
     */
    @RequestMapping("/addDish")
    public Map addOrder(@RequestParam(required = false) MultipartFile file, @RequestParam String dishname, @RequestParam Double money, @RequestParam int number, @RequestParam int state, HttpSession session) {
        String fileName="";
        if (file!=null){
            fileName = UUID.randomUUID().toString()+file.getOriginalFilename();
        }
        String filePath = ".\\src\\main\\resources\\static\\File\\";
        String RealfilePath = "File\\"+fileName;
        System.out.println(">>>>>"+RealfilePath+money);
        try {
            FileUtil.uploadFile(file.getBytes(), filePath, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        User user = (User) session.getAttribute("user");
        Dishes dishes = new Dishes();
        dishes.setNumber(number);
        dishes.setDishname(dishname);
        dishes.setImgurl(RealfilePath);
        if(user.getState()==1)
        money = money *0.9;
        dishes.setMoney(money);
        dishes.setState(state);
        boolean b = orderService.addOrder(dishes);
        StateSignal signal = new StateSignal();
        if (b) {
            signal.put(State.SuccessCode);
            signal.put(State.SuccessMessage);
        } else {
            signal.put(State.ErrorCode);
            signal.put(State.ErrorMessage);
        }

        return signal.getResult();
    }

    /**
     * 删除订单（管理员权限）
     *
     * @param orderid 订单id
     * @return
     */
    @RequestMapping("/delDish")
    public Map delOrder(@RequestParam int orderid) {
        boolean b = orderService.delOrder(orderid);
        StateSignal signal = new StateSignal();
        if (b) {
            signal.put(State.SuccessCode);
            signal.put(State.SuccessMessage);
        } else {
            signal.put(State.ErrorCode);
            signal.put(State.ErrorMessage);
        }
        return signal.getResult();
    }

    @RequestMapping("/getDishes")
    public Map selectDishes(@RequestParam int pageNum, @RequestParam int pageSize,@RequestParam(required = false,defaultValue = "-1") int state,@RequestParam(required = false)String serSearch1,@RequestParam(required = false,defaultValue = "-1")Double serSearch2) {
        IPage<Dishes> roomByState = orderService.getDishes(pageNum, pageSize, state,serSearch1,serSearch2);
        StateSignal signal = new StateSignal();
        if (roomByState != null) {
            signal.put(State.SuccessCode);
            signal.put(State.SuccessMessage);
            signal.put("List", roomByState.getRecords());
        } else {
            signal.put(State.ErrorCode);
            signal.put(State.ErrorMessage);
        }
        return signal.getResult();
    }

    @RequestMapping("/updateDish")
    public Map updateRoom(@RequestParam int dishid,@RequestParam(required = false,defaultValue = "0") double money,@RequestParam(required = false,defaultValue = "null") String dishname,@RequestParam(required = false,defaultValue = "-1") int state,@RequestParam(required = false,defaultValue = "-1") int number){
        Dishes dishes = new Dishes();
        dishes.setDishid(dishid);
        dishes.setMoney(money);
        dishes.setNumber(number);
        dishes.setState(state);
        dishes.setDishname(dishname);
        boolean b = orderService.updateDish(dishes);
        StateSignal signal = new StateSignal();
        if(b){
            signal.put(State.SuccessCode);
            signal.put(State.SuccessMessage);
        }else {
            signal.put(State.ErrorCode);
            signal.put(State.ErrorMessage);
        }

        return  signal.getResult();
    }

    /**
     * 修改订单的状态
     *

     * @return
     */
    @RequestMapping("/alterdish")
    public Map updateOrderState(@RequestParam int orderid, HttpSession session) {
        Dishes olddish = orderService.getDishesById(orderid);
        StateSignal signal = new StateSignal();
        if (olddish.getNumber() <= 0) {
            signal.put(State.ErrorCode);
            signal.put(State.Message3);
            return signal.getResult();
        }
        User user = (User) session.getAttribute("user");
        if (user.getMoney() < olddish.getMoney()) {
            signal.put(State.ErrorCode);
            signal.put(State.Message2);
            return signal.getResult();
        }
        user.setMoney(user.getMoney() - olddish.getMoney());
        userService.updateData(user);
        olddish.setNumber(olddish.getNumber() - 1);
        boolean b = orderService.updateDish(olddish);
       dishesListService.updateOrder(user,olddish);
        if (b) {
            signal.put(State.SuccessCode);
            signal.put(State.SuccessMessage);
        } else {
            signal.put(State.ErrorCode);
            signal.put(State.ErrorMessage);
        }
        roomOrder.getUserOrder(user);
        return signal.getResult();
    }

    @RequestMapping("/getAllOrder.do")
    public Map getAllOrder(@RequestParam int pageNum, @RequestParam int pageSize,HttpSession session) {
        User user = (User) session.getAttribute("user");
        List<DishesList> allOrder = orderService.getAllOrder(user.getUserid(),pageNum, pageSize);
        StateSignal signal = new StateSignal();
        if (allOrder != null) {
            signal.put(State.SuccessCode);
            signal.put(State.SuccessMessage);
            signal.put("List", allOrder);
            signal.put("pageNum", pageNum);
            signal.put("pageSize", pageSize);
        } else {
            signal.put(State.ErrorCode);
            signal.put(State.ErrorMessage);
        }
        return signal.getResult();
    }

}
