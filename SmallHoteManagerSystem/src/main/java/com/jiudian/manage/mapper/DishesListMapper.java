package com.jiudian.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiudian.manage.model.Dishes;
import com.jiudian.manage.model.DishesList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DishesListMapper extends BaseMapper<DishesList> {
    List<DishesList> getAllUser(int userid);
    DishesList getAllDish(@Param("userid")int userid,@Param("dishid") int dishid);
    boolean insertSelective(DishesList dishesList);
    boolean updateOrder(DishesList dishesList);
}