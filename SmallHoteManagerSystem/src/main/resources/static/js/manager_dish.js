var pageNum = 1;
var pageSize = 8;
var l;

$(document).ready(function () {
    getStaffList();
    $("#pre").on('click', function () {
        getPre();
    });
    $("#next").on('click', function () {
        getNext();
    });
    $("#addUserBtn").on('click', function () {
        uploadfile()
       // addUser();
    });
    $("input").filter("#setBtn").on('click',function( ){
        console.log("11111");
        setDishajax( );
    });
    $("#searchBtn").on('click',function(){
        getListSearch();
    });

})

//判断对象/JSON是否为空 空返回1 非空返回0
function isEmptyObject(e) {
    var t;
    for (t in e)
        return 0;
    return 1;
}

//判断字符串是否为空 空返回1 非空返回0
function isEmptyString(str) {
    if (str == 'null' || str == '')
        return 1;
    return 0;
}


var list;
function getListSearch() {
    var serSearch1 = $("#searchRoom").val();
    var serSearch2 = $("#searchMoney").val();
    $.ajax({
        type: "post",
        url: "../dish/getDishes",
        dataType: "JSON",
        data: {
            "pageNum": pageNum,
            "pageSize": pageSize,
            "serSearch1":serSearch1,
            "serSearch2":serSearch2
        },
        success: function (data) {
            if (data.code) {
                if (isEmptyObject(data.List) && pageNum > 0) {
                    pageNum = pageNum - 1;
                    getListSearch();
                }
                else {
                    var power = " ";
                    var htmlStr = " ";
                    var btnStr = " ";
                    list = data.List;
                    l = 0;
                    $("#pre").css("display", "block");
                    $("#next").css("display", "block");
                    $("#staffList").empty();
                    $("#staffList").append("<tr><th>菜单名</th><th>图片</th><th>数量</th><th>单价</th><th>状态</th><th>操作</th></tr>");
                    for (i in list) {
                        var url = '../'+list[i].imgurl;
                        var state = "今日提供";
                        if (list[i].state === 1) {
                            state = "今日不提供";
                        }
                        btnStr="<input type=\"button\" class=\"btn btn-success\"  data-toggle=\"modal\" data-target=\"#alterDish\"  data-userid=\""+list[i].dishid+"\" id=\"setUser\" value=\"编辑\">&nbsp;&nbsp;"+
                            "<input type=\"button\" id=\"delUser\" data-userid=\"" + list[i].dishid + "\" class=\"btn btn-danger\" value=\"删除\"/>";
                        htmlStr = "<tr data-userid=\"" + list[i].dishid + "\"><td>" + list[i].dishname + "</td><td><img style='width: 130px;height: 70px' src='"+url+"'+/><td>" + list[i].number + "</td><td>" + list[i].money + "</td><td>" + state + "</td><td>" + btnStr + "</td></tr>";
                        $("#staffList").append(htmlStr);
                        //console.log(htmlStr);
                        l++;
                    }
                    if (pageNum == "1") $("#pre").css("display", "none");
                    if (pageSize > l) $("#next").css("display", "none");
                    btnOn();
                }
            }
            else {
                alert("获取员工列表失败");
                var power = " ";
                var htmlStr = " ";
                var btnStr = " ";
                var state = " ";
                list = data.List;
                l = 0;
                $("#pre").css("display", "block");
                $("#next").css("display", "block");
                $("#staffList").empty();
                $("#staffList").append("<tr><th>菜单名</th><th>图片</th><th>数量</th><th>单价</th><th>状态</th><th>操作</th></tr>");
                btnStr="<input type=\"button\" class=\"btn btn-success\"  data-toggle=\"modal\" data-target=\"#alterDish\"  data-userid=\""+list[i].userid+"\" id=\"setUser\" value=\"修改信息\">&nbsp;&nbsp;"+
                    "<input type=\"button\" id=\"delUser\" data-userid=\"" + list[i].dishid + "\" class=\"btn btn-danger\" value=\"删除\"/>";
                htmlStr = "<tr data-userid=\"" + list[i].dishid + "\"><td>" + list[i].dishname + "</td><td>" + list[i].number + "</td><td>" + list[i].money + "</td><td>" + state + "</td><td>" + btnStr + "</td></tr>";
                $("#staffList").append(htmlStr);
                //console.log(htmlStr);
                l++;
            }
            if (pageNum == "1") $("#pre").css("display", "none");
            if (pageSize > l) $("#next").css("display", "none");
            btnOn();

        },
        error: function () {
            alert("获取菜单列表发生错误")
        }
    })
}
function getStaffList() {
    $.ajax({
        type: "post",
        url: "../dish/getDishes",
        dataType: "JSON",
        data: {
            "pageNum": pageNum,
            "pageSize": pageSize
        },
        success: function (data) {
            if (data.code) {
                if (isEmptyObject(data.List) && pageNum > 0) {
                    pageNum = pageNum - 1;
                    getStaffList();
                }
                else {
                    var power = " ";
                    var htmlStr = " ";
                    var btnStr = " ";
                    list = data.List;
                    l = 0;
                    $("#pre").css("display", "block");
                    $("#next").css("display", "block");
                    $("#staffList").empty();
                    $("#staffList").append("<tr><th>菜单名</th><th>图片</th><th>数量</th><th>单价</th><th>状态</th><th>操作</th></tr>");
                    for (i in list) {
                        var url = '../'+list[i].imgurl;
                        var state = "今日提供";
                        if (list[i].state === 1) {
                            state = "今日不提供";
                        }
                        btnStr="<input type=\"button\" class=\"btn btn-success\"  data-toggle=\"modal\" data-target=\"#alterDish\"  data-userid=\""+list[i].dishid+"\" id=\"setUser\" value=\"编辑\">&nbsp;&nbsp;"+
                            "<input type=\"button\" id=\"delUser\" data-userid=\"" + list[i].dishid + "\" class=\"btn btn-danger\" value=\"删除\"/>";
                        htmlStr = "<tr data-userid=\"" + list[i].dishid + "\"><td>" + list[i].dishname + "</td><td><img style='width: 130px;height: 70px' src='"+url+"'+/><td>" + list[i].number + "</td><td>" + list[i].money + "</td><td>" + state + "</td><td>" + btnStr + "</td></tr>";
                        $("#staffList").append(htmlStr);
                        //console.log(htmlStr);
                        l++;
                    }
                    if (pageNum == "1") $("#pre").css("display", "none");
                    if (pageSize > l) $("#next").css("display", "none");
                    btnOn();
                }
            }
            else {
                alert("获取员工列表失败");
                var power = " ";
                var htmlStr = " ";
                var btnStr = " ";
                var state = " ";
                list = data.List;
                l = 0;
                $("#pre").css("display", "block");
                $("#next").css("display", "block");
                $("#staffList").empty();
                $("#staffList").append("<tr><th>菜单名</th><th>数量</th><th>单价</th><th>状态</th><th>操作</th></tr>");
                btnStr="<input type=\"button\" class=\"btn btn-success\"  data-toggle=\"modal\" data-target=\"#alterDish\"  data-userid=\""+list[i].userid+"\" id=\"setUser\" value=\"修改信息\">&nbsp;&nbsp;"+
                    "<input type=\"button\" id=\"delUser\" data-userid=\"" + list[i].dishid + "\" class=\"btn btn-danger\" value=\"删除\"/>";
                htmlStr = "<tr data-userid=\"" + list[i].dishid + "\"><td>" + list[i].dishname + "</td><td>" + list[i].number + "</td><td>" + list[i].money + "</td><td>" + state + "</td><td>" + btnStr + "</td></tr>";
                $("#staffList").append(htmlStr);
                //console.log(htmlStr);
                l++;
            }
            if (pageNum == "1") $("#pre").css("display", "none");
            if (pageSize > l) $("#next").css("display", "none");
            btnOn();

        },
        error: function () {
            alert("获取菜单列表发生错误")
        }
    })
}

function btnOn() {
    $("input").filter("#delUser").on('click', function (event) {
        delUser(event);
    });
    $("input").filter("#setUser").on('click', function (event) {
        setUser(event);
    });
    $("input").filter("#setPageBtn").on('click', function () {
        setPage();
    });

}

function setDishajax( ){
    $.ajax({
        type:"POST",
        url:"../dish/updateDish",
        dataType:"JSON",
        data:{
            "dishid":$("#userid").val(),
            "money":$("#money").val(),
            "state":$("#disabledSelect").val(),
            "dishname":$("#dishname").val(),
            "number":$("#number").val()
        },
        success:function(data) {
            if (data.code == 0) {
                alert("修改成功");
                $("#userid").val("");
                $("#money").val("");
                $("#disabledSelect").val("");
                $("#dishname").val("");
                $("#number").val("");
                $('#alterDish').modal('toggle');
                getStaffList();
            } else {
                alert("修改失败");
                getStaffList();
            }
        },
        error:function(){
            alert("修改信息出现错误");
        }
    })
}
function setUser(event){
    var userid=$(event.target).data("userid");
    var info;
    for(i in list){
        if(list[i].dishid==userid){
            info=list[i];
        }
    }
    $("#useraccount").val(info.dishname);
    $("#password").val(info.state);
    $("#number").val(info.number);
    $("#dishname").val(info.dishname);
    $("#money").val(info.money);
    $("#userid").val(userid);
}
function getPre() {
    pageNum = pageNum - 1;
    getStaffList();
}

function getNext() {
    pageNum = pageNum + 1;
    getStaffList();
}


function setPage() {

    if ($("#inputPage").val() < 0 || $("#inputPage").val() == 0)
        alert("请输入正确页码");
    else {
        pageNum = $("#inputPage").val();
        getStaffList();
    }

}


function delUser(event) {
    var userid = $(event.target).data("userid");
    $.ajax({
        type: "POST",
        url: "../dish/delDish",
        dataType: "JSON",
        data: {
            "orderid": userid
        },
        success: function (data) {
            if (data.code == 0) {
                alert("删除成功");
                if (l == 1)
                    pageNum = pageNum - 1;
                getStaffList();
            }
        },
        error: function () {
            alert("删除出现错误");
        }
    })

}

// function addUser() {
//     if (isEmptyString($("#dishname1").val()) || isEmptyString($("#money1").val()))
//         alert("请填写全内容");
//     else {
//         $.ajax({
//             type: "POST",
//             url: "../dish/addDish",
//             dataType: "JSON",
//             data: {
//                 "dishname": $("#dishname1").val(),
//                 "money": $("#money1").val(),
//                 "number": $("#number1").val(),
//                 "state": $("#disabledSelect1").val()
//             },
//             success: function (data) {
//                 if (data.code == 0) {
//                     alert("添加成功");
//                     $('#addUser').modal('toggle');
//                     $("#inputAccount").val("");
//                     $("#inputPwd").val("")
//                     getStaffList();
//                 }
//                 else
//                     alert("添加失败")
//             },
//             error: function () {
//                 alert("系统出现错误");
//             }
//         })
//     }
//
// }
function uploadfile() {
    var formData = new FormData();
    formData.append("file",$('#inputFile')[0].files[0]);
    formData.append("dishname",$("#dishname1").val());
    formData.append("money",$("#money1").val());
    formData.append("number",$("#number1").val());
    formData.append("state",$("#disabledSelect1").val());
    $.ajax({
        type:'POST',
        url: "../dish/addDish",
        data:formData,
        processData: false,
        cache:false,
        contentType: false,
        mimeType:"multipart/form-data",
        success:function (data) {
            setTimeout(function () {
                alert("保存成功");
                window.location.reload()

            },1000)
        }
    })
}