package com.yanzhen.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.pagehelper.PageInfo;
import com.yanzhen.model.Notice;
import com.yanzhen.model.NoticeView;
import com.yanzhen.model.Userinfo;
import com.yanzhen.service.INoticeService;
import com.yanzhen.service.INoticeViewService;
import com.yanzhen.util.JsonObject;
import com.yanzhen.util.StatusDefind;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author kappy
 * @since 2020-11-08
 */
@Api(tags = {""})
@RestController
@RequestMapping("/notice")
public class NoticeController {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Resource
    private INoticeService noticeService;

    @Resource
    private INoticeViewService noticeViewService;

    @ApiOperation(value = "新增")
    @PostMapping()
    public int add(@RequestBody Notice notice) {
        return noticeService.add(notice);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public int delete(@PathVariable("id") Long id) {
        return noticeService.delete(id);
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public int update(@RequestBody Notice notice) {
        return noticeService.updateData(notice);
    }

    @ApiOperation(value = "查询分页数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码"),
            @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping()
    public IPage<Notice> findListByPage(@RequestParam Integer page,
                                        @RequestParam Integer pageCount) {
        return noticeService.findListByPage(page, pageCount);
    }

    @GetMapping("/findListByPage")
    public JsonObject<NoticeView> findListByPage2(@RequestParam Integer page,
                                                  @RequestParam Integer limit) {
        JsonObject object = new JsonObject();
        PageInfo<NoticeView> pageInfo = noticeViewService.findListByPage(page, limit);
        object.setCode(0);
        object.setMsg("ok");
        object.setCount(pageInfo.getTotal());
        object.setData(pageInfo.getList());
        return object;

    }
    @ApiOperation(value = "id查询")
    @RequestMapping("/allmax")
    public void allmax() throws InterruptedException {
        Long aLong= noticeService.findMax();
        StatusDefind.Noticeid = aLong;
        System.out.println("++++++++++allmax"+ aLong);

    }

    @ApiOperation(value = "id查询")
    @RequestMapping("/checkStatus")
    public Map maxById() throws InterruptedException {
        NoticeView notice = noticeViewService.findmax(StatusDefind.Noticeid);
        Map map = new HashMap();
        JsonObject object=new JsonObject();
        if (notice != null){
            map.put("code", 200);
            map.put("notice", notice.getAddr()+"火灾等级: "+notice.getStatus());
            StatusDefind.Noticeid = notice.getId();
        } else {
            map.put("code", 404);
            map.put("notice", "null");
        }

        return map;
    }

    @ApiOperation(value = "id查询")
    @RequestMapping("/changeStatus")
    public void findById(@RequestParam int status) throws InterruptedException {
        System.out.println(">>>>>>>>>>" + status);
        Map map = new HashMap();

        if (status == 1) {
            StatusDefind.status = 1;
        } else {
            StatusDefind.status = 0;
        }
        Random r = new Random(1);
        while (StatusDefind.status == 1) {
            System.out.println(">>>>>>>>>>" + StatusDefind.status);
            int ran1 = r.nextInt(100);
            int ran2 = r.nextInt(5);
            ran2 = ran2 == 0 ? 1 : ran2;
            if (ran1 >= 95) {
                Notice notice = new Notice();
                notice.setContent("test" + ran1);
                notice.setFbdate(new Date());
                notice.setIdhouse(ran2);
                notice.setStatus(3);
                noticeService.add(notice);
            } else if (ran1 >= 90) {
                Notice notice = new Notice();
                notice.setContent("test" + ran1);
                notice.setFbdate(new Date());
                notice.setIdhouse(ran2);
                notice.setStatus(2);
                noticeService.add(notice);
            } else if (ran1 <= 90 && ran1 >= 85) {
                Notice notice = new Notice();
                notice.setContent("test" + ran1);
                notice.setFbdate(new Date());
                notice.setIdhouse(ran2);
                notice.setStatus(1);
                noticeService.add(notice);
            }
            Thread.currentThread().sleep(2000);
        }
    }


}
