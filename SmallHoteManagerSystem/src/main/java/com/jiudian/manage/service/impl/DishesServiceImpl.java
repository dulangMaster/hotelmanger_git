package com.jiudian.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.jiudian.manage.mapper.*;
import com.jiudian.manage.model.*;
import com.jiudian.manage.service.DishesService;
import com.jiudian.manage.until.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishesServiceImpl extends ServiceImpl<DishesMapper, Dishes> implements DishesService {
    @Autowired
    OrderMapper orderMapper;

    @Autowired
    DishesListMapper dishesList;
    @Autowired
    RoomMapper roomMapper;
    @Autowired
    ConfigMapper configMapper;

    @Override
    public boolean addOrder(String householdname, String id, String starttime, String endtime, int roomid, int userid) {
        Room room = roomMapper.selectByPrimaryKey(roomid);
        if (room.getState() != 1) {
            return false;
        }
        Order order = new Order();
        order.setHouseholdname(householdname);
        order.setId(id);
        order.setStarttime(TimeUtil.formatterTime(starttime));
        order.setEndtime(TimeUtil.formatterTime(endtime));
        order.setRoomid(roomid);
        order.setUserid(userid);
        order.setState(0);
        double money = TimeUtil.getBetweenDay(starttime, endtime) * room.getMoney();
        order.setMoney(money);

        Config config = configMapper.selectByPrimaryKey(1);
        config.setTotalroom(config.getTotalroom() + 1);
        config.setTotalmoney(config.getTotalmoney() + money);
        configMapper.updateByPrimaryKeySelective(config);

        int insert = orderMapper.insertSelective(order);
        if (insert > 0) {
            Room room1 = new Room();
            room1.setRoomid(roomid);
            room1.setState(2);
            int i = roomMapper.updateByPrimaryKeySelective(room1);
            if (i > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean addOrder(Dishes dishes) {
        int i = baseMapper.insert(dishes);
        return (i == 0) ? false : true;
    }

    @Override
    public boolean delOrder(int orderid) {
      int i = baseMapper.deleteById(orderid);
        return (i == 0) ? false : true;
    }

    @Override
    public boolean updateOrderState(int orderid, int state) {
        Order order = orderMapper.selectByPrimaryKey(orderid);
        if (order == null) {
            return false;
        }
        Integer roomid = order.getRoomid();
        Room room = new Room();
        room.setRoomid(roomid);
        int i = 1;
        if (state == 2) {
            room.setState(3);
            i = roomMapper.updateByPrimaryKeySelective(room);
        }
        if (state == 3) {
            room.setState(1);
            i = roomMapper.updateByPrimaryKeySelective(room);
        }
        order.setState(state);
        if (i > 0) {
            int i1 = orderMapper.updateByPrimaryKeySelective(order);
            if (i1 > 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean updateDish(Dishes dishes) {
        int i =  baseMapper.updateById(dishes);
        return i==0?false:true;
    }

    @Override
    public List<DishesList> getAllOrder(int userid, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return dishesList.getAllUser(userid);
    }

    @Override
    public IPage<Dishes> getDishes(int pageNum, int pageSize, int state,String name,Double money) {
        IPage<Dishes> wherePage = new Page<>(pageNum, pageSize);

        QueryWrapper<Dishes> queryWrapper = new QueryWrapper<>();
        if (state != -1){
            queryWrapper.eq("state",state);
        }
        if (name != null){
            queryWrapper.like("dishname",name);
        }
        if (money != -1){
            queryWrapper.eq("money",money);
        }
        return baseMapper.selectPage(wherePage, queryWrapper);
    }

    @Override
    public Dishes getDishesById(int id) {
        return baseMapper.selectById(id);
    }
}
