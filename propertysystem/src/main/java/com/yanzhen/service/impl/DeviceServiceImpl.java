package com.yanzhen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yanzhen.dao.DeviceMapper;
import com.yanzhen.dao.DeviceMapper;
import com.yanzhen.model.Device;
import com.yanzhen.model.Device;
import com.yanzhen.model.Owner;
import com.yanzhen.service.IDeviceService;
import com.yanzhen.service.IDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kappy
 * @since 2020-11-08
 */
@Service
public class DeviceServiceImpl extends ServiceImpl<DeviceMapper, Device> implements IDeviceService {

    @Autowired
    private DeviceMapper DeviceMapper;

    public PageInfo<Device> findListByPage(Integer page, Integer pageSize) {

        PageHelper.startPage(page,pageSize);
        //查询的结果集
        List<Device> list=DeviceMapper.queryUserinfoAll();
        PageInfo<Device> pageInfo=new PageInfo<>(list);
        return  pageInfo;

    }
  public   PageInfo<Device> findOwnerAll(int page, int pagesize, Device owner){
        PageHelper.startPage(page,pagesize);
        //查询的结果集
        List<Device> list=DeviceMapper.queryUserinfoAll();
        PageInfo<Device> pageInfo=new PageInfo<>(list);
        return  pageInfo;
    }
    @Override
    public int add(Device owner){
        return baseMapper.insert(owner);
    }

    @Override
    public int updateData(Device owner){
        return baseMapper.updateById(owner);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }
    @Override
    public Device findById(Long id){
        return  baseMapper.selectById(id);
    }
    public void updateOwer(Integer id){
        DeviceMapper.updateOwer(id);
    }

}

