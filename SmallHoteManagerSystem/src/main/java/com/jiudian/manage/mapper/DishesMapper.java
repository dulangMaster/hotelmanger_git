package com.jiudian.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiudian.manage.model.Dishes;
import com.jiudian.manage.model.DishesList;
import com.jiudian.manage.model.Order;
import com.jiudian.manage.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DishesMapper  extends BaseMapper<Dishes> {

}