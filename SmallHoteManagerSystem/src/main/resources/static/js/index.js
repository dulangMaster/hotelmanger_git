$(document).ready(function(){
	getCode();
	var h=$(window).height();
	$("#main").css("height",h);
	$("#loginBtn").click(function(){
		login();
	})
	$("#code").click(function(){
		getCode();
	});
    $("#addUserBtn").on('click',function(){
        uploadfile();
       // addUser();
    });
});


//获取验证码
function getCode(){
	$("#code").attr("src","./user/createImage?code="+Math.random());
}
function addUser(){
    if($("#phonenumber1").val().length < 8 || $("#phonenumber1").val().length > 12) {
        alert("联系方式格式有错");
        return false;
    }
    if(isEmptyString($("#inputAccount").val())||isEmptyString($("#inputPwd").val()))
        alert("请填写全内容");
    else{
        $.ajax({
            type:"POST",
            url:"../user/addUser.do",
            dataType:"JSON",
            data:{
                "useraccount":$("#inputAccount").val(),
                "password":$("#inputPwd").val(),
                "username":$("#username1").val(),
                "age":$("#age1").val(),
                "money":$("#money1").val(),
                "phonenumber":$("#phonenumber1").val(),
                "gender":$("input[name='optradio']:checked").val(),
                "power":"0"
            },
            success:function(data){
                if(data.code==0){
                    alert("添加成功");
                    $('#addUser').modal('toggle');
                    $("#inputAccount").val("");
                    $("#inputPwd").val("")
                }
                else
                    alert("添加失败")
            },
            error:function(){
                alert("添加用户出现错误");
            }
        })
    }
}
//登录
function login(){
	var user=$("#inputName").val();
	var pwd=$("#inputPassword").val();
	var code=$("#inputCode").val();
	$.ajax({
		type:"POST",
		url:"./user/login.do",
		dataType:"JSON",
		data:{
			"useraccount":user,
			"password":pwd,
			"icode":code
		},
		success:function(data){
			console.log(data)
			if(data.code=="0"){
				var urlString="pages/myCenter.html?power="+data.power+"&userid="+data.userid;
				window.location.href=urlString;
			}
			else if(data.code=="-1"){
				alert("验证码或密码错误")
			}
		},
		error:function(){
			alert("登录 发生错误");
		}
	});
}
function isEmptyString(str){
    if(str=='null'||str=='')
        return 1;
    return 0;
}

function uploadfile() {
    var formData = new FormData();
    formData.append("file",$('#inputFile')[0].files[0]);
    formData.append("useraccount",$('#inputAccount').val());
    formData.append("password",$('#inputPwd').val());
    formData.append("username",$('#username1').val());
    formData.append("age",$('#age1').val());
    formData.append("money",$('#money1').val());
    formData.append("phonenumber",$('#phonenumber1').val());
    formData.append("gender",$("input[name='optradio']:checked").val());
    formData.append("power",0);
    $.ajax({
        type:'POST',
        url :'/upFile/upFile',
        data:formData,
        processData: false,
        cache:false,
        contentType: false,
        mimeType:"multipart/form-data",
        success:function (data) {
            setTimeout(function () {
                window.location.reload()
            },1000)
        }
    })
}