package com.yanzhen.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yanzhen.model.Owner;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kappy
 * @since 2021-03-31
 */
@Component("ownerDao")
public interface OwnerMapper extends BaseMapper<Owner> {
    //查询
    List<Owner> queryOwnerAll(Owner owner);

    List<Owner> queryOwner(@Param("tel") String tel,@Param("identity") String identity,@Param("houseId") String houseId);

    Owner queryOwnerByName(@Param("username") String username);

}
