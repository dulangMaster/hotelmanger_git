(function ($) {
    $.getData = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]+)(&|$)?");
        var result = window.location.search.substr(1).match(reg);
        if (result != null) return result[2];
        return null;
    }
})(jQuery);

var power = $.getData('power');
var userid = $.getData('userid');


$(document).ready(function () {
    $("#mainFrame").attr("src", "all_infor.html?userid=" + userid + "&power=" + power);
    $("inputNone").attr("value", userid);
    var h = $(window).height();
    $("#tagList").css("height", h);
    $("#mainFrame").css("height", h);
    setList();
})

function setList() {
    var tagList = " ";
    if (power == "1") {
        tagList ="<a href=\"all_infor.html?userid=" + userid + "&power=" + power + "\" target=\"mainFrame\" class=\"list-group-item active\"><span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>个人信息</a>" +
        "<a href=\"manage_staff.html\" target=\"mainFrame\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-list\" aria-hidden=\"true\"></span>用户管理</a>" +
        "<a href=\"manage_room.html\"  target=\"mainFrame\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-home\" aria-hidden=\"true\"></span>房间管理</a>" +
        "<a href=\"manage_dishes.html\"  target=\"mainFrame\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-home\" aria-hidden=\"true\"></span>菜谱管理</a>" +
        "<a href=\"ad_order.html\"  target=\"mainFrame\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-shopping-cart\" aria-hidden=\"true\"></span> 订单管理</a>"

    } else  {
        tagList = "<a href=\"all_infor.html?userid=" + userid + "&power=" + power + "\"  target=\"mainFrame\" class=\"list-group-item active\"><span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>个人信息</a>" +
            "<a href=\"staff_room.html?userid=" + userid + "\" target=\"mainFrame\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-home\" aria-hidden=\"true\"></span>客房预定</a>" +
            "<a href=\"staff_order.html\"  target=\"mainFrame\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-shopping-cart\" aria-hidden=\"true\"></span>客房订单管理</a>" +
            "<a href=\"staff_dishes.html?userid=" + userid + "\" target=\"mainFrame\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-home\" aria-hidden=\"true\"></span>餐饮购买</a>"+
            "<a href=\"staff_dishesOrder.html\"  target=\"mainFrame\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-shopping-cart\" aria-hidden=\"true\"></span>餐饮订单管理</a>"
    }
    $("#tagList").append(tagList);
    tagList = $("#tagList").children("a");
    tagList.on('click', function (event) {
        changeColor(event)
    });
}

function changeColor(event) {
    var obj = event.target;
    var objSi = $(obj).siblings();
    $(obj).attr("class", "list-group-item active");
    $(objSi).attr("class", "list-group-item ");
}