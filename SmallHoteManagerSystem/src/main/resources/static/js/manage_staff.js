var pageNum=1;
var pageSize=8;
var l;

$(document).ready(function(){
	getStaffList();
	$("#pre").on('click',function(){
		getPre();
	});
	$("#next").on('click',function(){
		getNext();
	});
	$("#addUserBtn").on('click',function(){
        uploadfile()
		//addUser();
	});
    $("input").filter("#setRoomBtn").on('click',function( ){
        setRoomajax( );
    });
    $("#searchBtn").on('click',function(){
        getListSearch();
    });

})

//判断对象/JSON是否为空 空返回1 非空返回0
function isEmptyObject(e) {
	var t;
	for (t in e)
		return 0;
	return 1;
}

//判断字符串是否为空 空返回1 非空返回0
function isEmptyString(str){
	if(str=='null'||str=='')
		return 1;
	return 0;
}



var list;
function getListSearch(){
    var serSearch1 = $("#searchRoom").val();
    var serSearch2 = $("#searchMoney").val();
    $.ajax({
        type:"post",
        url:"../user/getUserByPower.do",
        dataType:"JSON",
        data:{
            "pageNum":pageNum,
            "pageSize":pageSize,
            "power":"0",
            "serSearch1":serSearch1,
            "serSearch2":serSearch2
        },
        success:function(data){
            if(data.code=="0"){
                if(isEmptyObject(data.List)&&pageNum>0){
                    pageNum=pageNum-1;
                    getStaffList();
                }
                else{
                    var power=" ";
                    var htmlStr=" ";
                    var btnStr=" ";
                    list=data.List;
                    l=0;
                    $("#pre").css("display","block");
                    $("#next").css("display","block");
                    $("#staffList").empty();
                    $("#staffList").append("<tr><th>账号</th><th>姓名</th><th>年龄</th><th>充值金额</th><th>联系方式</th><th>操作</th></tr>")
                    for(i in list){
                        btnStr="<input type=\"button\" class=\"btn btn-success\"  data-toggle=\"modal\" data-target=\"#alterUser\"  data-userid=\""+list[i].userid+"\" id=\"setUser\" value=\"修改信息\">&nbsp;&nbsp;"
                            +"<input type=\"button\" id=\"delUser\" data-userid=\""+list[i].userid+"\" class=\"btn btn-danger\" value=\"删除\"/>"
                        htmlStr="<tr data-userid=\""+list[i].userid+"\"><td>"+list[i].useraccount+"</td><td>"+list[i].username+"</td><td>"+list[i].age+"</td><td>"+list[i].money+"</td><td>"+list[i].phonenumber+"</td><td>"+btnStr+"</td></tr>";
                        $("#staffList").append(htmlStr);
                        //console.log(htmlStr);
                        l++;
                    }
                    if(pageNum=="1") $("#pre").css("display","none");
                    if(pageSize>l) $("#next").css("display","none");
                    btnOn();
                }
            }
            else{
                alert("获取员工列表失败");
            }


        },
        error:function(){
            alert("获取员工列表发生错误")
        }
    })
}
function getStaffList(){
	$.ajax({
		type:"post",
		url:"../user/getUserByPower.do",
		dataType:"JSON",
		data:{
			"pageNum":pageNum,
			"pageSize":pageSize,
			"power":"0"
		},
		success:function(data){
			if(data.code=="0"){
				if(isEmptyObject(data.List)&&pageNum>0){
					pageNum=pageNum-1;
					getStaffList();
				}
				else{
					var power=" ";
					var htmlStr=" ";
					var btnStr=" ";
					list=data.List;
					l=0;
					$("#pre").css("display","block");
					$("#next").css("display","block");
					$("#staffList").empty();
					$("#staffList").append("<tr><th>账号</th><th>姓名</th><th>年龄</th><th>充值金额</th><th>联系方式</th><th>操作</th></tr>")
					for(i in list){
                        btnStr="<input type=\"button\" class=\"btn btn-success\"  data-toggle=\"modal\" data-target=\"#alterUser\"  data-userid=\""+list[i].userid+"\" id=\"setUser\" value=\"修改信息\">&nbsp;&nbsp;"
						+"<input type=\"button\" id=\"delUser\" data-userid=\""+list[i].userid+"\" class=\"btn btn-danger\" value=\"删除\"/>"
						htmlStr="<tr data-userid=\""+list[i].userid+"\"><td>"+list[i].useraccount+"</td><td>"+list[i].username+"</td><td>"+list[i].age+"</td><td>"+list[i].money+"</td><td>"+list[i].phonenumber+"</td><td>"+btnStr+"</td></tr>";
						$("#staffList").append(htmlStr);
						//console.log(htmlStr);
						l++;
					}
					if(pageNum=="1") $("#pre").css("display","none");
					if(pageSize>l) $("#next").css("display","none");
					btnOn();
				}
			}
			else{
				alert("获取员工列表失败");
			}
			

		},
		error:function(){
			alert("获取员工列表发生错误")
		}
	})
}


function btnOn(){
	$("input").filter("#delUser").on('click',function(event){
		delUser(event);
	});
	$("input").filter("#setPageBtn").on('click',function( ){
		setPage( );
	});
	$("input").filter("#setUser").on('click',function(event){

       setUser(event);
    });

}

function getPre(){
	pageNum=pageNum-1;
	getStaffList();
}

function getNext(){
	pageNum=pageNum+1;
	getStaffList();	
}


function setPage(){
	
	if($("#inputPage").val()<0 || $("#inputPage").val()==0)
		alert("请输入正确页码");
	else{
		pageNum=$("#inputPage").val();
		getStaffList();
	}
}


function setUser(event){
    var userid=$(event.target).data("userid");
    var info;
    for(i in list){
        if(list[i].userid==userid){
            info=list[i];
        }
    }
    $("#useraccount").val(info.useraccount);
    $("#password").val(info.password);
    $("#username").val(info.username);
    $("#age").val(info.age);
    $("#money").val(info.money);
    $("#phonenumber").val(info.phonenumber);
    $("#userid").val(userid);
}


function delUser(event){
	var userid=$(event.target).data("userid");
	alert("userid"+userid);
	$.ajax({
		type:"POST",
		url:"../user/delUser.do",
		dataType:"JSON",
		data:{
			"userid":userid
		},
		success:function(data){
				if(data.code==0){
					alert("删除成功");
				if(l==1)
					pageNum=pageNum-1;
				getStaffList();
			}
			else
				alert("删除失败")
		},
		error:function(){
			alert("删除出现错误");
		}
	})

}

function addUser(){
	if($("#phonenumber1").val().length < 8 || $("#phonenumber1").val().length > 12) {
        alert("联系方式格式有错");
        return false;
	}
	if(isEmptyString($("#inputAccount").val())||isEmptyString($("#inputPwd").val()))
		alert("请填写全内容");
	else{
		$.ajax({
			type:"POST",
			url:"../user/addUser.do",
			dataType:"JSON",
			data:{
				"useraccount":$("#inputAccount").val(),
				"password":$("#inputPwd").val(),
                "username":$("#username1").val(),
                "age":$("#age1").val(),
                "money":$("#money1").val(),
                "phonenumber":$("#phonenumber1").val(),
				"power":"0"
			},
			success:function(data){
				if(data.code==0){
					alert("添加成功");
					$('#addUser').modal('toggle');
					$("#inputAccount").val("");
					$("#inputPwd").val("")
					getStaffList();
				}
				else
					alert("添加失败")
			},
			error:function(){
				alert("添加用户出现错误");
			}
		})
	}
	
}


function setRoomajax( ){
    if($("#phonenumber").val().length < 8 || $("#phonenumber").val().length > 12) {
        alert("联系方式格式有错");
        return false;
    }
    $.ajax({
        type:"POST",
        url:"../user/updateUser.do",
        dataType:"JSON",
        data:{
            "userid":$("#userid").val(),
            "money":$("#money").val(),
            "age":$("#age").val(),
            "username":$("#username").val(),
            "phonenumber":$("#phonenumber").val(),
			"power":0
        },
        success:function(data){
            if(data.code==0){
                alert("修改成功");
                $("#reinputLocal").val("");
                $("#reinputPrice").val("");
                $("#reinputType").val("");
                $("#reinputid").val("");
                $('#alterUser').modal('toggle');
                getStaffList();
            }
            else{
                alert("修改失败");
                getStaffList();
            }
        },
        error:function(){
            alert("修改信息出现错误");
        }
    })
}

function uploadfile() {
    var formData = new FormData();
    formData.append("file",$('#inputFile')[0].files[0]);
    formData.append("useraccount",$('#inputAccount').val());
    formData.append("password",$('#inputPwd').val());
    formData.append("username",$('#username1').val());
    formData.append("age",$('#age1').val());
    formData.append("money",$('#money1').val());
    formData.append("phonenumber",$('#phonenumber1').val());
    formData.append("gender",$("input[name='optradio']:checked").val());
    formData.append("power",0);
    $.ajax({
        type:'POST',
        url :'/upFile/upFile',
        data:formData,
        processData: false,
        cache:false,
        contentType: false,
        mimeType:"multipart/form-data",
        success:function (data) {
            setTimeout(function () {
                window.location.reload()
            },1000)
        }
    })
}