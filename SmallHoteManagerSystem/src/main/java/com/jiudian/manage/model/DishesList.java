package com.jiudian.manage.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class DishesList {
    @TableId(value = "userid", type = IdType.AUTO)
    private Integer orderid;

    private String dishname;

    private String username;
    private Double money;

    private Integer state;

    private Integer number;


    private Integer dishid;

    private Integer userid;


}