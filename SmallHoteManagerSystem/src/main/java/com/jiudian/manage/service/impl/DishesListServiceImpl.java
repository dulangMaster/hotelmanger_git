package com.jiudian.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.jiudian.manage.mapper.*;
import com.jiudian.manage.model.*;
import com.jiudian.manage.service.DishesListService;
import com.jiudian.manage.service.DishesService;
import com.jiudian.manage.until.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class DishesListServiceImpl extends ServiceImpl<DishesListMapper, DishesList> implements DishesListService {
   @Autowired
   DishesListMapper dishesListMapper;

    @Override
    public boolean updateOrder(User user, Dishes dish) {

        DishesList dishes = dishesListMapper.getAllDish(user.getUserid(),dish.getDishid());
        System.out.println(">>>>>>>>>dishes" +dishes);
        if (dishes!=null){
            dishes.setMoney(dish.getMoney() + dishes.getMoney());
            dishes.setNumber( dishes.getNumber()+1);
            dishesListMapper.updateOrder(dishes);
        }else {
            DishesList dishes2 = new DishesList();
            dishes2.setMoney(dish.getMoney());
            dishes2.setNumber(1);
            dishes2.setDishid(dish.getDishid());
            dishes2.setUserid(user.getUserid());
            dishesListMapper.insertSelective(dishes2);
        }
        return false;
    }

}
